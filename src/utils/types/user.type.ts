export type user = {
    _id: string,
    name: string,
    email: string,
    age: number,
    rol: string,
    katas: []
}