import { AxiosResponse } from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getAllKatas } from "../services/katasService";
import { kata } from "../utils/types/Kata.type";
import { CreateKataPage } from "./createKataPage";


// Material UI

import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import CameraIcon from '@mui/icons-material/PhotoCamera';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { height } from "@mui/system";


// Tab setup

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    {children}
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export const KatasPage = () => {

    let loggedIn = useSessionStorage('sessionJWTToken');
    let navigate = useNavigate();

    const [katas, setKatas] = useState([]); // initial katas its empty
    const [totalPages, setTotalPages] = useState(4); // initial default value
    const [currentPage, setCurrentPage] = useState(1); // initial default value

    // Tab Handle
    const [tabValue, setTabValue] = React.useState(0);

    const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
        setTabValue(newValue);
    };

    const theme = createTheme();

    useEffect(() => {
        if (!loggedIn) {
            return navigate('/login');
        } else {
            getAllKatas(loggedIn, totalPages, currentPage).then((response: AxiosResponse) => {

                if (response.status === 200 && response.data.katas && response.data.totalPages && response.data.currentPage) {
                    console.table(response.data)

                    let { katas, totalPages, currentPage } = response.data

                    setKatas(katas)
                    setTotalPages(totalPages)
                    setCurrentPage(currentPage)
                } else {
                    throw new Error(`Error obtaining katas ${response}`)
                }

            }).catch((error) => console.error(`[GET ALL KATAS ERROR]: ${error}`))
        }
    }, [loggedIn])

    /**
     * Method to navigate to kata details
     * @param {string} id of kata to navigate to
     */
    const navigateToKataDetail = (id: string) => {
        navigate(`/katas/${id}`);
    }

    const listOfKatas = () => {
        return (
            < Container sx={{ py: 8 }
            } maxWidth="md" >
                {/* End hero unit */}
                < Grid container spacing={4} >
                    {
                        katas.map((kata: kata, index) => (
                            <Grid item key={index} xs={12} sm={6} >
                                <Card
                                    sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                                >
                                    <CardContent sx={{ flexGrow: 1 }}>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {kata.name}
                                        </Typography>
                                        <Typography>
                                            {kata.description}
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button size="small" onClick={() => navigateToKataDetail(kata._id)}>View Kata</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        ))
                    }
                </Grid >
            </Container >
        )
    }

    return (
        <div>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Box sx={{ width: '100%', height: '100%' }}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs value={tabValue} onChange={handleTabChange}>
                            <Tab label="Katas" {...a11yProps(0)} />
                            <Tab label="Create new Kata" {...a11yProps(1)} />
                        </Tabs>
                    </Box>
                    <TabPanel value={tabValue} index={0}>
                        <Container sx={{ py: 8 }} maxWidth="md">
                            <main>
                                {katas.length > 0 ?
                                    <div>
                                        {listOfKatas()}
                                    </div>
                                    :
                                    <div>
                                        <h5>
                                            No Katas found
                                        </h5>
                                    </div>
                                }
                            </main>
                        </Container>
                    </TabPanel>
                    <TabPanel value={tabValue} index={1}>
                        <CreateKataPage />
                    </TabPanel>
                </Box>
            </ThemeProvider>
        </div>
    )
}
