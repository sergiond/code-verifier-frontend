import React from "react";
import CreateKataForm from "../components/forms/createKataForm";
import CreateKataFormMaterial from "../components/forms/createKataFormMat";


export const CreateKataPage = () => {

    return (
        <div>
            {/**<CreateKataForm /> */}
            <CreateKataFormMaterial />
        </div>
    )
}