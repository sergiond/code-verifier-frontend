import React from "react";

import LoginForm from "../components/forms/loginForm";
import LoginFormMaterial from "../components/forms/loginFormMaterial";


export const LoginPage = () => {

    return (
        <div>
            {/*<LoginForm /> */}
            <LoginFormMaterial />
        </div>
    )
}
