import { AxiosResponse } from "axios";
import React, { useEffect, useState } from "react";
//React Router DOM imports
import { useNavigate, useParams } from 'react-router-dom';
import RegisterFormMaterial from "../components/forms/registerFormMaterial";
import UpdateUserFormMaterial from "../components/forms/updateUserFormMaterial";

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getUserByID } from "../services/userService";
import { user } from "../utils/types/user.type";

export const UserDetailPage = () => {

    // This is far for be the right solution to persist user's id and rol, only for testing
    let loggedIn = useSessionStorage('sessionJWTToken');
    let userRol = useSessionStorage('sessionTokenRol')

    // Variable to navegate between stack of routes
    let navigate = useNavigate();
    // Fin id from params
    let { id } = useParams();

    const [user, setUser] = useState<user | any>(undefined);


    useEffect(() => {
        if (!loggedIn || userRol !== 'admin') {
            return navigate('/');
        } else {
            if (id) {
                getUserByID(loggedIn, id).then((response: AxiosResponse) => {
                    if (response.status === 200 && response.data) {
                        let { _id, name, email, age, rol, katas } = response.data
                        let userData: user = {
                            _id: _id,
                            name: name,
                            email: email,
                            age: age,
                            rol: rol,
                            katas: katas
                        }
                        setUser(userData)
                        console.table(userData);
                    }

                }).catch((error) => console.error(`[User by ID ERROR]: ${error}`))
            } else {
                return navigate('/users');
            }
        }
    }, [loggedIn])


    return (
        <div>
            {user ?
                <UpdateUserFormMaterial {...user} />
                :
                <div>
                    <h2> Loading data...</h2>
                </div>
            }
        </div>
    )
}