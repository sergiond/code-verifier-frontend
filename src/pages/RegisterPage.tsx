import React from "react";
import RegisterForm from "../components/forms/registerForm";
import RegisterFormMaterial from "../components/forms/registerFormMaterial";

export const RegisterPage = () => {

    return (
        <div>
            {/*<RegisterForm /> */}
            <RegisterFormMaterial />
        </div>
    )
}