import { AxiosResponse } from "axios";
import React, { useEffect, useState } from "react";
//React Router DOM imports
import { useNavigate, useParams } from 'react-router-dom';
import { NewEditor } from "../components/editor/NewEditor";

import { useSessionStorage } from "../hooks/useSessionStorage";
import { getKataByID } from "../services/katasService";
import { kata } from "../utils/types/Kata.type";
import Rater from "react-rater";
import { Box, ThemeProvider } from "@mui/system";
import { Grid, CssBaseline, createTheme, Paper, Typography } from "@mui/material";
import UpdateKataFormMaterial from "../components/forms/updateKataFormMaterial";

export const KatasDetailPage = () => {

    // This is far for be the right solution to persist user's id and rol, only for testing
    let loggedIn = useSessionStorage('sessionJWTToken');
    let kataUserID = useSessionStorage('sessionTokenID')
    let userRol = useSessionStorage('sessionTokenRol')

    // Variable to navegate between stack of routes
    let navigate = useNavigate();
    // Fin id from params
    let { id } = useParams();

    const theme = createTheme();

    const [kata, setKata] = useState<kata | any>(undefined);


    useEffect(() => {
        if (!loggedIn) {
            return navigate('/login');
        } else {
            if (id) {
                getKataByID(loggedIn, id).then((response: AxiosResponse) => {
                    if (response.status === 200 && response.data) {
                        let { _id, name, description, level, intents, stars, creator, solution, participants } = response.data
                        let kataData: kata = {
                            _id: _id,
                            name: name,
                            description: description,
                            level: level,
                            intents: intents,
                            stars: stars,
                            creator: creator,
                            solution: '',
                            participants: participants
                        }
                        setKata(kataData)
                        console.table(kataData);
                    }

                }).catch((error) => console.error(`[Kata by ID ERROR]: ${error}`))
            } else {
                return navigate('/katas');
            }
        }
    }, [id, loggedIn, navigate])


    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '150vh', alignItems: 'center' }}>
                <CssBaseline />
                <Grid item sx={{ margin: 'auto', }} xs={12} component={Paper} elevation={0} square >
                    {kata ?
                        <Box>
                            <Typography component="h2" variant="h3">{kata?.name}</Typography>
                            <Typography sx={{ my: 1 }} component="h3" variant="h5">{kata?.description}</Typography>
                            <Typography sx={{ mb: 2 }} component="h5" variant="h5">Rating: {<Rater total={5} rating={kata?.stars} interactive={false} />}</Typography>
                            <NewEditor >{kata}</NewEditor>

                        </Box>
                        :
                        <Box >
                            <Typography component="h1" variant="h5">Loading data...</Typography>
                        </Box>
                    }
                    {
                        kataUserID === kata?.creator || userRol === 'admin' ?
                            <UpdateKataFormMaterial {...kata} />
                            :
                            null
                    }
                </Grid>
            </Grid>
        </ThemeProvider>
    )
}
