import { Fragment, useState } from "react";
import { Dropzone, FileItem, FileValidated, FullScreenPreview, VideoPreview } from "@dropzone-ui/react";

export const FileUploader = () => {

    const [files, setFiles] = useState<FileValidated[]>([]);
    const [videoSrc, setVideoSrc] = useState<any>(undefined);
    const [imageSrc, setImageSrc] = useState<any>(undefined);

    const updateFiles = (incommingFiles: FileValidated[]) => {
        setFiles(incommingFiles);
    };
    const onDelete = (id: string | number | undefined) => {
        setFiles(files.filter((x) => x.id !== id));
    };
    const handleWatch = (vidSrc: File | undefined) => {
        setVideoSrc(vidSrc);
    };
    const handleSee = (imageSource: string | File | undefined) => {
        setImageSrc(imageSource);
    };

    return (
        <Fragment>
            <Dropzone
                onChange={updateFiles}
                value={files}
                footer={false}
                label="Archivos permitidos .zip, .rar, .pdf, .pptx, .docx, .xlsx, .mp4, .png, .jpg y .jpeg"
                maxFiles={3}
                accept=".zip,.rar,.pdf,.pptx,.docx,.xlsx,.mp4,.png,.jpg,.jpeg"
                maxFileSize={209720000}
                fakeUploading
                disableScroll
            >
                {files.map((file) => (
                    <FileItem
                        {...file}
                        key={file.id}
                        onDelete={onDelete}
                        onWatch={handleWatch}
                        onSee={handleSee}
                        localization="ES-es"
                        preview
                        info
                        hd
                        alwaysActive
                        resultOnTooltip
                    />
                ))}
            </Dropzone>
            <VideoPreview
                videoSrc={videoSrc}
                openVideo={videoSrc}
                onClose={(e: any) => handleWatch(undefined)}
                controls
                autoplay
            />
            <FullScreenPreview
                imgSource={imageSrc}
                openImage={imageSrc}
                onClose={(e: any) => handleSee(undefined)}
            />
        </Fragment>
    );
}