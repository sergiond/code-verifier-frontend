import React from 'react';
import { useNavigate } from 'react-router-dom';

import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';

import { login } from '../../services/authService';
import { AxiosResponse } from 'axios';

// Material UI



// Define Schema of validation with Yup
const loginSchema = yup.object().shape({
    email: yup.string()
        .email('Invalid Email Format')
        .required('Email is required'),
    password: yup.string()
        .required('Password is required')
});

// Login component

const LoginForm = () => {

    // We define the initial credentials
    const initialCredentials = {
        email: '',
        password: ''
    }

    let navigate = useNavigate();

    return (
        <div>
            <h4>Login Form</h4>
            <Formik
                initialValues={initialCredentials}
                validationSchema={loginSchema}
                onSubmit={async (values) => {

                    login(values.email, values.password).then(async (response: AxiosResponse) => {
                        if (response.status === 200) {
                            console.table(response.data)
                            if (response.data.token) {
                                await sessionStorage.setItem('sessionJWTToken', response.data.id)
                                navigate('/');
                            } else {
                                throw new Error('Error generating login token');
                            }
                        } else {
                            throw new Error('Invalid Credential');
                        }
                    }).catch((error) => console.error(`[LOGIN ERROR]: Something went wrong ${error}`))

                }}
            >
                {
                    ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
                        <Form>
                            { /*Email Field */}
                            <label htmlFor='email'>Email</label>
                            <Field id='email' type='email' name='email' placeholder='example@email.com' />
                            { /*Email errors */}
                            {
                                errors.email && touched.email && (
                                    <ErrorMessage name='email' component='div'></ErrorMessage>
                                )
                            }

                            { /*Password Field */}
                            <label htmlFor='password'>Password</label>
                            <Field id='password' type='password' name='password' placeholder='Example Password' />
                            { /*Password errors */}
                            {
                                errors.password && touched.password && (
                                    <ErrorMessage name='password' component='div'></ErrorMessage>
                                )
                            }

                            { /*SUBMIT FORM */}
                            <button type='submit'>Login</button>

                            { /*Message if the form is submitting */}
                            {
                                isSubmitting ? (<div>Checking credentials...</div>) : null
                            }
                        </Form>
                    )
                }

            </Formik>
        </div>
    )

}

export default LoginForm