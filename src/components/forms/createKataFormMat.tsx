import React from 'react';
import { AxiosResponse } from 'axios';
import { useFormik } from 'formik';
import * as yup from 'yup';



import { useSessionStorage } from "../../hooks/useSessionStorage";
import { createKata } from "../../services/katasService";
import { updateUserKatasById } from '../../services/userService';


// Material UI

import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { FormControl, InputBase, InputLabel, MenuItem, Select, SelectChangeEvent, Typography } from '@mui/material';
// import { FileUploader } from '../uploader/FileUploader';

const theme = createTheme();

// Define Schema of validation with Yup

const KataSchema = yup.object().shape({
    name: yup.string()
        .min(6, 'User Katas name must have 3 letters minimun')
        .max(48, 'User Katas name must have máximun 24 letters')
        .required('kata name is required'),
    description: yup.string()
        .min(12, 'User Katas description must have 12 letters minimun')
        .max(512, 'User Katas description must have máximun 128 letters')
        .required('kata description is required'),
    level: yup.string()
        .required('kata level is required'),
    solution: yup.string()
        .min(12, 'User Katas solution must have 12 letters minimun')
        .max(512, 'User Katas solution must have máximun 512 letters')
        .required('kata solution is required'),
});

const CreateKataFormMat = () => {

    let loggedIn = useSessionStorage('sessionJWTToken');
    let sessionUserID = useSessionStorage('sessionTokenID');
    let action = 'add';

    // we define the initial values of the kata
    const initialKataValues = {
        name: 'Kata prueba añadir a usuario',
        description: 'Kata prueba añadir a usuario',
        level: 'Basic',
        intents: 0,
        stars: 0,
        solution: 'Kata prueba añadir a usuario'
    }

    const [level, setLevel] = React.useState('');
    const handleChangeLevel = (event: SelectChangeEvent) => {
        setLevel(event.target.value);
    };

    const formik = useFormik({
        initialValues: initialKataValues,
        validationSchema: KataSchema,
        onSubmit: async (values) => {
            console.log(values);
            createKata(
                loggedIn,
                values.name,
                values.description,
                level,
                values.intents,
                values.stars,
                values.solution
            ).then((response: AxiosResponse) => {
                if (response.status === 201) {
                    if (response) {
                        //add to user the new Kata ID
                        updateUserKatasById(loggedIn, sessionUserID, action, response.data.id)
                        alert('Kata created correctly')
                    } else {
                        throw new Error('Error reading User token');
                    }
                } else {
                    throw new Error('Invalid Kata creation data');
                }
            }).catch((error) => console.error(`[KATA CREATION ERROR]: Something went wrong ${error}`))
        }
    });

    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh', alignItems: 'center' }}>
                <CssBaseline />
                <Grid item sx={{ margin: 'auto' }} xs={12} sm={8} md={5} component={Paper} elevation={0} square >
                    <Box
                        sx={{
                            my: 8,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Typography variant="h4" component="h3">
                            Create New Kata
                        </Typography>
                        <form noValidate onSubmit={formik.handleSubmit} >

                            { /*Kata name Field */}

                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="name"
                                name="name"
                                label="name"
                                value={formik.values.name}
                                onChange={formik.handleChange}
                                error={formik.touched.name && Boolean(formik.errors.name)}
                                helperText={formik.touched.name && formik.errors.name}
                            />


                            { /*Description Field */}
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="description"
                                name="description"
                                label="description"
                                value={formik.values.description}
                                onChange={formik.handleChange}
                                error={formik.touched.description && Boolean(formik.errors.description)}
                                helperText={formik.touched.description && formik.errors.description}
                            />

                            { /*Kata Level Field */}
                            <FormControl fullWidth>
                                <InputLabel id="level">Level</InputLabel>
                                <Select
                                    required
                                    fullWidth
                                    id="level"
                                    name="level"
                                    label="level"
                                    defaultValue={"Basic"}
                                    value={level}
                                    onChange={handleChangeLevel}
                                    error={formik.touched.level && Boolean(formik.errors.level)}
                                >
                                    <MenuItem value="Basic">Basic</MenuItem >
                                    <MenuItem value="Medium">Medium</MenuItem >
                                    <MenuItem value="High">High</MenuItem >
                                </Select>
                            </FormControl>

                            { /*Solution Field */}
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="solution"
                                name="solution"
                                label="solution"
                                value={formik.values.solution}
                                onChange={formik.handleChange}
                                error={formik.touched.solution && Boolean(formik.errors.solution)}
                                helperText={formik.touched.solution && formik.errors.solution}
                            />
                            { /*SUBMIT FORM */}
                            <Grid item sx={{ mt: 2 }}>
                                <Button
                                    color="primary"
                                    variant="contained"
                                    fullWidth
                                    type="submit">
                                    Submit
                                </Button>
                            </Grid>
                        </form>
                    </Box>
                </Grid>
            </Grid>
        </ThemeProvider>
    )
}

export default CreateKataFormMat