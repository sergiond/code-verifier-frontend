import React from 'react';
import { AxiosResponse } from 'axios';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';


import { useSessionStorage } from "../../hooks/useSessionStorage";
import { deleteKataByID, updateKata } from "../../services/katasService";
import { useNavigate, useParams } from 'react-router-dom';
import { kata } from '../../utils/types/Kata.type';
import { updateUserKatasById } from '../../services/userService';

// Define Schema of validation with Yup

const KataSchema = yup.object().shape({
    name: yup.string()
        .min(6, 'User Katas name must have 3 letters minimun')
        .max(48, 'User Katas name must have máximun 24 letters'),
    description: yup.string()
        .min(12, 'User Katas description must have 12 letters minimun')
        .max(512, 'User Katas description must have máximun 512 letters'),
    level: yup.string(),
    solution: yup.string()
        .min(12, 'User Katas solution must have 12 letters minimun')
        .max(512, 'User Katas solution must have máximun 512 letters'),
});

const UpdateKataForm = (kata: kata) => {

    let loggedIn = useSessionStorage('sessionJWTToken');
    let sessionUserID = useSessionStorage('sessionTokenID');
    let action = 'remove';
    // Fin id from params
    let { id }: any = useParams();
    let navigate = useNavigate();


    // we define the initial values of the kata
    const initialKataValues = {
        name: kata.name,
        description: kata.description,
        level: kata.level,
        creator: kata.creator,
        solution: kata.solution
    }

    const deleteKata = () => {


        deleteKataByID(loggedIn, id)
            .then((response: AxiosResponse) => {
                if (response.status === 200) {
                    if (response) {
                        console.log('Kata deleted correctly');
                        updateUserKatasById(loggedIn, sessionUserID, action, id);
                        navigate(`/katas`);
                    } else {
                        throw new Error('Error reading User token');
                    }
                } else {
                    throw new Error('Invalid Kata id');
                }
            }).catch((error) => console.error(`[DELETE KATA ERROR]: Something went wrong ${error}`))
    }

    return (
        <div>
            <h4> Update Kata</h4>
            <Formik
                initialValues={initialKataValues}
                validationSchema={KataSchema}
                onSubmit={async (values) => {
                    if (id) {
                        updateKata(
                            loggedIn,
                            id,
                            sessionUserID,
                            values.name,
                            values.description,
                            values.level,
                            initialKataValues.creator,
                            values.solution,
                            'NoAction'
                        ).then((response: AxiosResponse) => {
                            if (response.status === 200) {
                                if (response) {
                                    alert('Kata updated correctly');
                                    navigate(`/katas`);
                                } else {
                                    throw new Error('Error reading User token');
                                }
                            } else {
                                throw new Error('Invalid Kata update data');
                            }
                        }).catch((error) => console.error(`[UPDATE KATA ERROR]: Something went wrong ${error}${values.creator}`))
                    }
                }
                }
            >
                {
                    ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
                        <Form>

                            { /*Kata name Field */}
                            <label htmlFor='name'>Kata name</label>
                            <Field id='name' type='text' name='name' placeholder='Kata name' />
                            { /*Kata name errors */}
                            {
                                errors.name && touched.name && (
                                    <ErrorMessage name='name' component='div'></ErrorMessage>
                                )
                            }

                            { /*Description Field */}
                            <label htmlFor='description'>Description</label>
                            <Field id='description' type='text' name='description' placeholder='Kata description' />
                            { /*Description errors */}
                            {
                                errors.description && touched.description && (
                                    <ErrorMessage name='description' component='div'></ErrorMessage>
                                )
                            }

                            { /*Kata Level Field */}
                            <label htmlFor='level'>Kata Level</label>
                            <Field id='level' type='text' name='level' placeholder='Kata Level' />
                            { /*Kata Level errors */}
                            {
                                errors.level && touched.level && (
                                    <ErrorMessage name='level' component='div'></ErrorMessage>
                                )
                            }

                            { /*Solution Field */}
                            <label htmlFor='solution'>Solution</label>
                            <Field id='solution' type='text' name='solution' placeholder='solution' />
                            { /*Solution errors */}
                            {
                                errors.solution && touched.solution && (
                                    <ErrorMessage name='solution' component='div'></ErrorMessage>
                                )
                            }
                            { /*SUBMIT FORM */}
                            <button type='submit'>Update Kata</button>

                            { /*Message if the form is submitting */}
                            {
                                isSubmitting ? (<div>Sending data to update new Kata...</div>) : null
                            }
                        </Form>
                    )
                }
            </Formik>
            <div>
                <button onClick={deleteKata}>
                    Delete Kata
                </button>
            </div>
        </div>
    )
}

export default UpdateKataForm