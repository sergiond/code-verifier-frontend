import React from 'react';
import { AxiosResponse } from 'axios';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';

import { register } from '../../services/authService';


// Define Schema of validation with Yup
const RegistSchema = yup.object().shape({
    name: yup.string()
        .min(3, 'User Name must have 3 letters minimun')
        .max(12, 'User Name must have máximun 12 letters')
        .required('Name is required'),
    email: yup.string()
        .email('Invalid Email Format')
        .required('Email is required'),
    password: yup.string()
        .min(8, 'User Password must have 8  characters minimun')
        .required('Password is required'),
    passwordConfirmation: yup.string()
        .when('password', {
            is: (value: string) => (value && value.length > 0 ? true : false),
            then: yup.string().oneOf(
                [yup.ref('password')], 'Passwords must match'
            )
        })
        .required('Password confirmation is required'),
    age: yup.number()
        .min(10, 'you mush be over 10 years old')
        .required('Age is required')
});

// Register component

const RegisterForm = () => {

    // We define the initial credentials
    const initialCredentials = {
        name: '',
        email: '',
        password: '',
        passwordConfirmation: '',
        rol: '',
        age: 18
    }

    return (
        <div>
            <h4>Register Form</h4>
            <Formik
                initialValues={initialCredentials}
                validationSchema={RegistSchema}
                onSubmit={async (values) => {
                    register(values.name, values.email, values.password, values.rol, values.age).then((response: AxiosResponse) => {
                        if (response.status === 201) {
                            if (response) {
                                console.log('User registered correctly')
                                console.log(response.data)
                                alert('User registered correctly')
                            } else {
                                throw new Error('Error reading Registration token');
                            }
                        } else {
                            throw new Error('Invalid Registation data');
                        }
                    }).catch((error) => console.error(`[REGIST ERROR]: Something went wrong ${error}`))
                }}
            >
                {
                    ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
                        <Form>

                            { /*Name Field */}
                            <label htmlFor='name'>name</label>
                            <Field id='name' type='text' name='name' placeholder='Your Name' />
                            { /*Name errors */}
                            {
                                errors.name && touched.name && (
                                    <ErrorMessage name='name' component='div'></ErrorMessage>
                                )
                            }

                            { /*Email Field */}
                            <label htmlFor='email'>Email</label>
                            <Field id='email' type='email' name='email' placeholder='example@email.com' />
                            { /*Email errors */}
                            {
                                errors.email && touched.email && (
                                    <ErrorMessage name='email' component='div'></ErrorMessage>
                                )
                            }

                            { /*Password Field */}
                            <label htmlFor='password'>Password</label>
                            <Field id='password' type='password' name='password' placeholder='Password' />
                            { /*Password errors */}
                            {
                                errors.password && touched.password && (
                                    <ErrorMessage name='password' component='div'></ErrorMessage>
                                )
                            }

                            { /*Password Field */}
                            <label htmlFor='passwordConfirmation'>Password</label>
                            <Field id='passwordConfirmation' type='password' name='passwordConfirmation' placeholder='Confirm your password' />
                            { /*Password errors */}
                            {
                                errors.passwordConfirmation && touched.passwordConfirmation && (
                                    <ErrorMessage name='passwordConfirmation' component='div'></ErrorMessage>
                                )
                            }

                            { /*Age Field */}
                            <label htmlFor='age'>age</label>
                            <Field id='age' type='number' name='age' />
                            { /*Age errors */}
                            {
                                errors.age && touched.age && (
                                    <ErrorMessage name='age' component='div'></ErrorMessage>
                                )
                            }
                            { /*SUBMIT FORM */}
                            <button type='submit'>Register</button>

                            { /*Message if the form is submitting */}
                            {
                                isSubmitting ? (<div>Sending data to registry...</div>) : null
                            }
                        </Form>
                    )
                }

            </Formik>
        </div>
    )

}

export default RegisterForm