import React from 'react';
import { AxiosResponse } from 'axios';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup';

import { useNavigate } from 'react-router-dom';

import { useSessionStorage } from "../../hooks/useSessionStorage";
import { createKata } from "../../services/katasService";
import { updateUserKatasById } from '../../services/userService';
import { NewEditor } from '../editor/NewEditor';

// Define Schema of validation with Yup

const KataSchema = yup.object().shape({
    name: yup.string()
        .min(6, 'User Katas name must have 3 letters minimun')
        .max(48, 'User Katas name must have máximun 24 letters')
        .required('kata name is required'),
    description: yup.string()
        .min(12, 'User Katas description must have 12 letters minimun')
        .max(512, 'User Katas description must have máximun 128 letters')
        .required('kata description is required'),
    level: yup.string()
        .required('kata level is required'),
    solution: yup.string()
        .min(12, 'User Katas solution must have 12 letters minimun')
        .max(512, 'User Katas solution must have máximun 512 letters')
        .required('kata solution is required'),
});

const CreateKataForm = () => {

    let loggedIn = useSessionStorage('sessionJWTToken');
    let sessionUserID = useSessionStorage('sessionTokenID');
    let action = 'add';

    // we define the initial values of the kata
    const initialKataValues = {
        name: 'Kata prueba añadir a usuario',
        description: 'Kata prueba añadir a usuario',
        level: 'Basic',
        intents: 0,
        stars: 0,
        solution: 'Kata prueba añadir a usuario'
    }

    return (
        <div>
            <Formik
                initialValues={initialKataValues}
                validationSchema={KataSchema}
                onSubmit={async (values) => {
                    console.log(values);
                    createKata(
                        loggedIn,
                        values.name,
                        values.description,
                        values.level,
                        values.intents,
                        values.stars,
                        values.solution
                    ).then((response: AxiosResponse) => {
                        if (response.status === 201) {
                            if (response) {
                                console.log('Kata created correctly');
                                console.log(`Token: ${loggedIn}`);
                                console.log(`Session User ID: ${sessionUserID}`);
                                console.table(response.data.id);
                                //add to user the new Kata ID
                                updateUserKatasById(loggedIn, sessionUserID, action, response.data.id)
                                alert('Kata created correctly')
                            } else {
                                throw new Error('Error reading User token');
                            }
                        } else {
                            throw new Error('Invalid Kata creation data');
                        }
                    }).catch((error) => console.error(`[KATA CREATION ERROR]: Something went wrong ${error}`))
                }}
            >
                {
                    ({ values, touched, errors, isSubmitting, handleChange, handleBlur }) => (
                        <Form>

                            { /*Kata name Field */}
                            <label htmlFor='name'>Kata name</label>
                            <Field id='name' type='text' name='name' placeholder='Kata Name' />
                            { /*Kata name errors */}
                            {
                                errors.name && touched.name && (
                                    <ErrorMessage name='name' component='div'></ErrorMessage>
                                )
                            }

                            { /*Description Field */}
                            <label htmlFor='description'>Description</label>
                            <Field id='description' type='text' name='description' placeholder='Kata description' />
                            { /*Description errors */}
                            {
                                errors.description && touched.description && (
                                    <ErrorMessage name='description' component='div'></ErrorMessage>
                                )
                            }

                            { /*Kata Level Field */}
                            <label htmlFor='level'>Kata Level</label>
                            <Field id='level' type='text' name='level' placeholder='Kata Level' />
                            { /*Kata Level errors */}
                            {
                                errors.level && touched.level && (
                                    <ErrorMessage name='level' component='div'></ErrorMessage>
                                )
                            }

                            { /*Solution Field */}
                            <label htmlFor='solution'>Solution</label>
                            <Field id='solution' type='text' name='solution' placeholder='solution' />
                            { /*Solution errors */}
                            {
                                errors.solution && touched.solution && (
                                    <ErrorMessage name='solution' component='div'></ErrorMessage>
                                )
                            }
                            { /*SUBMIT FORM */}
                            <button type='submit'>Create new Kata</button>

                            { /*Message if the form is submitting */}
                            {
                                isSubmitting ? (<div>Sending data to create new Kata...</div>) : null
                            }
                        </Form>
                    )
                }
            </Formik>
        </div>
    )
}

export default CreateKataForm