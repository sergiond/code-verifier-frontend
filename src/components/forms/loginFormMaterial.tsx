import React from 'react';
import { useNavigate } from 'react-router-dom';

import { useFormik } from 'formik';
import * as yup from 'yup';

import { login } from '../../services/authService';
import { AxiosResponse } from 'axios';


// Material UI
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme();

// Define Schema of validation with Yup
const loginSchema = yup.object().shape({
    email: yup.string()
        .email('Invalid Email Format')
        .required('Email is required'),
    password: yup.string()
        .required('Password is required')
});

const LoginFormMaterial = () => {

    // We define the initial credentials
    const initialCredentials = {
        email: 'sergio@prueba.com',
        password: 'SergioPass'
    }

    let navigate = useNavigate();

    const formik = useFormik({
        initialValues: initialCredentials,
        validationSchema: loginSchema,
        onSubmit: async (values) => {
            login(values.email, values.password).then(async (response: AxiosResponse) => {
                if (response.status === 200) {
                    if (response.data.token) {
                        await sessionStorage.setItem('sessionJWTToken', response.data.token)
                        await sessionStorage.setItem('sessionTokenID', response.data.id)
                        await sessionStorage.setItem('sessionTokenRol', response.data.rol)
                        navigate('/');
                    } else {
                        throw new Error('Error generating login token');
                    }
                } else {
                    throw new Error('Invalid Credential');
                }
            }).catch((error) => { console.error(`[LOGIN ERROR]: Something went wrong ${error}`); alert('User not registered') })
        },
    });


    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh' }}>
                <CssBaseline />
                <Grid
                    item
                    xs={false}
                    sm={4}
                    md={7}
                    sx={{
                        backgroundImage: 'url(https://source.unsplash.com/random)',
                        backgroundRepeat: 'no-repeat',
                        backgroundColor: (t) =>
                            t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                    }}
                />
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                    <Box
                        sx={{
                            my: 8,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign in
                        </Typography>
                        <Box sx={{ mt: 1 }}>
                            <form noValidate onSubmit={formik.handleSubmit} >
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    name="email"
                                    label="Email"
                                    value={formik.values.email}
                                    onChange={formik.handleChange}
                                    error={formik.touched.email && Boolean(formik.errors.email)}
                                    helperText={formik.touched.email && formik.errors.email}
                                />
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="password"
                                    name="password"
                                    label="Password"
                                    type="password"
                                    value={formik.values.password}
                                    onChange={formik.handleChange}
                                    error={formik.touched.password && Boolean(formik.errors.password)}
                                    helperText={formik.touched.password && formik.errors.password}
                                />
                                <Grid item sx={{ mt: 2 }}>
                                    <Button
                                        color="primary"
                                        variant="contained"
                                        fullWidth
                                        type="submit">
                                        Submit
                                    </Button>
                                </Grid>
                            </form>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </ThemeProvider>
    );
};

export default LoginFormMaterial;