import React from 'react';
import { useNavigate } from 'react-router-dom';

import { useFormik } from 'formik';
import * as yup from 'yup';

import { AxiosResponse } from 'axios';


// Material UI
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { FormControl, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { user } from '../../utils/types/user.type';
import { updateUserById } from '../../services/userService';
import { useSessionStorage } from '../../hooks/useSessionStorage';

const theme = createTheme();

// Define Schema of validation with Yup
const RegistSchema = yup.object().shape({
    name: yup.string()
        .min(3, 'User Name must have 3 letters minimun')
        .max(12, 'User Name must have máximun 12 letters')
        .required('Name is required'),
    email: yup.string()
        .email('Invalid Email Format')
        .required('Email is required'),
    rol: yup.string(),
    age: yup.number()
        .min(10, 'you mush be over 10 years old')
        .required('Age is required')
});

const UpdateUserFormMaterial = (user: user) => {

    let loggedIn = useSessionStorage('sessionJWTToken');
    // We define the initial credentials
    const initialCredentials = {
        name: user.name,
        email: user.email,
        password: '',
        passwordConfirmation: '',
        rol: '',
        age: user.age
    }


    let navigate = useNavigate();

    const [userRol, setUserRol] = React.useState<string>(initialCredentials.rol);
    const handleChangeRol = (event: SelectChangeEvent) => {
        setUserRol(event.target.value);
    };

    function handdleLoginForm() {
        navigate('/login');
    };


    const formik = useFormik({
        initialValues: initialCredentials,
        validationSchema: RegistSchema,
        onSubmit: async (values) => {
            updateUserById(loggedIn, user._id, values.name, values.email, values.password, userRol, values.age).then((response: AxiosResponse) => {
                if (response.status === 200) {
                    if (response) {
                        alert('User updated ok')
                        navigate('/')
                    } else {
                        throw new Error('Error reading Update token');
                    }
                } else {
                    throw new Error('Invalid Registation data or user already exist');
                }
            }).catch((error) => console.error(`[REGIST ERROR]: Something went wrong ${error}`))
        }
    });

    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '100vh', alignItems: 'center' }}>
                <CssBaseline />
                <Grid item sx={{ margin: 'auto' }} xs={12} sm={8} md={5} component={Paper} elevation={0} square >
                    <Box
                        sx={{
                            my: 8,
                            mx: 4,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Register
                        </Typography>
                        <Box sx={{ mt: 1 }}>
                            <form noValidate onSubmit={formik.handleSubmit} >
                                <Grid container spacing={2}>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            fullWidth
                                            id="name"
                                            name="name"
                                            label="name"
                                            value={formik.values.name}
                                            onChange={formik.handleChange}
                                            error={formik.touched.name && Boolean(formik.errors.name)}
                                            helperText={formik.touched.name && formik.errors.name}
                                            autoFocus
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <TextField
                                            required
                                            fullWidth
                                            id="age"
                                            name="age"
                                            label="age"
                                            value={formik.values.age}
                                            onChange={formik.handleChange}
                                            error={formik.touched.age && Boolean(formik.errors.age)}
                                            helperText={formik.touched.age && formik.errors.age}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="email"
                                            name="email"
                                            label="Email"
                                            value={formik.values.email}
                                            onChange={formik.handleChange}
                                            error={formik.touched.email && Boolean(formik.errors.email)}
                                            helperText={formik.touched.email && formik.errors.email}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField
                                            required
                                            fullWidth
                                            id="password"
                                            name="password"
                                            label="Password"
                                            type="password"
                                            value={formik.values.password}
                                            onChange={formik.handleChange}
                                            error={formik.touched.password && Boolean(formik.errors.password)}
                                            helperText={formik.touched.password && formik.errors.password}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField
                                            required
                                            fullWidth
                                            id="passwordConfirmation"
                                            name="passwordConfirmation"
                                            label="Password Confirmation"
                                            type="password"
                                            value={formik.values.passwordConfirmation}
                                            onChange={formik.handleChange}
                                            error={formik.touched.passwordConfirmation && Boolean(formik.errors.passwordConfirmation)}
                                            helperText={formik.touched.passwordConfirmation && formik.errors.passwordConfirmation}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControl fullWidth>
                                            <InputLabel id="rol">Rol</InputLabel>
                                            <Select
                                                required
                                                fullWidth
                                                id="rol"
                                                name="rol"
                                                label="rol"
                                                onChange={handleChangeRol}
                                                value={userRol}
                                                error={formik.touched.rol && Boolean(formik.errors.rol)}
                                            >
                                                <MenuItem value="user">User</MenuItem >
                                                <MenuItem value="admin">Admin</MenuItem >
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                </Grid>
                                <Grid item sx={{ mt: 2 }}>
                                    <Button
                                        color="primary"
                                        variant="contained"
                                        fullWidth
                                        type="submit">
                                        Submit
                                    </Button>
                                </Grid>
                            </form>
                        </Box>
                    </Box>
                </Grid>
            </Grid>
        </ThemeProvider>
    );
};

export default UpdateUserFormMaterial;