import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import { useFormik } from 'formik';
import * as yup from 'yup';

import { AxiosResponse } from 'axios';

//Type
import { kata } from '../../utils/types/Kata.type';

// Material UI
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { FormControl, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material';
import { updateUserKatasById } from '../../services/userService';
import { useSessionStorage } from '../../hooks/useSessionStorage';
import { deleteKataByID, updateKata } from '../../services/katasService';

const theme = createTheme();

// Define Schema of validation with Yup
const KataSchema = yup.object().shape({
    name: yup.string()
        .min(6, 'User Katas name must have 3 letters minimun')
        .max(48, 'User Katas name must have máximun 24 letters'),
    description: yup.string()
        .min(12, 'User Katas description must have 12 letters minimun')
        .max(512, 'User Katas description must have máximun 512 letters'),
    level: yup.string(),
    solution: yup.string()
        .min(12, 'User Katas solution must have 12 letters minimun')
        .max(512, 'User Katas solution must have máximun 512 letters'),
});

const UpdateKataFormMaterial = (kata: kata) => {

    let loggedIn = useSessionStorage('sessionJWTToken');
    let sessionUserID = useSessionStorage('sessionTokenID');
    let action = 'remove';
    // Fin id from params
    let { id }: any = useParams();
    let navigate = useNavigate();


    // we define the initial values of the kata
    const initialKataValues = {
        name: kata.name,
        description: kata.description,
        level: kata.level,
        creator: kata.creator,
        solution: kata.solution
    }

    const deleteKata = () => {
        deleteKataByID(loggedIn, id)
            .then((response: AxiosResponse) => {
                if (response.status === 200) {
                    if (response) {
                        console.log('Kata deleted correctly');
                        updateUserKatasById(loggedIn, sessionUserID, action, id);
                        navigate(`/katas`);
                    } else {
                        throw new Error('Error reading User token');
                    }
                } else {
                    throw new Error('Invalid Kata id');
                }
            }).catch((error) => console.error(`[DELETE KATA ERROR]: Something went wrong ${error}`))
    }

    const [kataLevel, setKataLevel] = React.useState<string>(initialKataValues.level);
    const handleChangeLevel = (event: SelectChangeEvent) => {
        setKataLevel(event.target.value);
    };

    const formik = useFormik({
        initialValues: initialKataValues,
        validationSchema: KataSchema,
        onSubmit: async (values) => {
            if (id) {
                updateKata(
                    loggedIn,
                    id,
                    sessionUserID,
                    values.name,
                    values.description,
                    values.level,
                    initialKataValues.creator,
                    values.solution,
                    'NoAction'
                ).then((response: AxiosResponse) => {
                    if (response.status === 200) {
                        if (response) {
                            alert('Kata updated correctly');
                            navigate(`/katas`);
                        } else {
                            throw new Error('Error reading User token');
                        }
                    } else {
                        throw new Error('Invalid Kata update data');
                    }
                }).catch((error) => console.error(`[UPDATE KATA ERROR]: Something went wrong ${error}${values.creator}`))
            }
        }
    });

    return (
        <ThemeProvider theme={theme}>
            <Grid container component="main" sx={{ height: '50vh', alignItems: 'center' }}>
                <CssBaseline />
                <Grid item sx={{ margin: 'auto' }} xs={12} sm={8} md={5} component={Paper} elevation={0} square >
                    <Box >
                        <Typography component="h1" variant="h5">
                            Manage Katas
                        </Typography>
                        <Box sx={{ mt: 2 }}>
                            <form noValidate onSubmit={formik.handleSubmit} >
                                <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <TextField
                                            required
                                            fullWidth
                                            id="name"
                                            name="name"
                                            label="name"
                                            value={formik.values.name}
                                            onChange={formik.handleChange}
                                            error={formik.touched.name && Boolean(formik.errors.name)}
                                            helperText={formik.touched.name && formik.errors.name}
                                            autoFocus
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField
                                            required
                                            fullWidth
                                            id="description"
                                            name="description"
                                            label="description"
                                            value={formik.values.description}
                                            onChange={formik.handleChange}
                                            error={formik.touched.description && Boolean(formik.errors.description)}
                                            helperText={formik.touched.description && formik.errors.description}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControl fullWidth>
                                            <InputLabel id="level">Rol</InputLabel>
                                            <Select
                                                required
                                                fullWidth
                                                id="level"
                                                name="level"
                                                label="leve"
                                                onChange={handleChangeLevel}
                                                value={kataLevel}
                                                error={formik.touched.level && Boolean(formik.errors.level)}
                                            >
                                                <MenuItem value="Basic">Basic</MenuItem >
                                                <MenuItem value="Medium">Medium</MenuItem >
                                                <MenuItem value="Hight">Hight</MenuItem >
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField
                                            required
                                            fullWidth
                                            id="solution"
                                            name="solution"
                                            label="solution"
                                            type="solution"
                                            value={formik.values.solution}
                                            onChange={formik.handleChange}
                                            error={formik.touched.solution && Boolean(formik.errors.solution)}
                                            helperText={formik.touched.solution && formik.errors.solution}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item sx={{ mt: 2 }}>
                                    <Button
                                        color="primary"
                                        variant="contained"
                                        fullWidth
                                        type="submit">
                                        Submit
                                    </Button>
                                </Grid>
                            </form>
                        </Box>
                    </Box>
                    <Box sx={{ mt: 4 }}>
                        <Button
                            variant="outlined"
                            color="error"
                            fullWidth
                            onClick={deleteKata}
                        >
                            Delete Kata
                        </Button>
                    </Box>
                </Grid>
            </Grid>
        </ThemeProvider>
    );
};

export default UpdateKataFormMaterial;