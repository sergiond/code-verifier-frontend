import React from "react";

// Material List Components
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";

// Material Icon Components
import PersonIcon from '@mui/icons-material/Person';
import DashboardIcon from '@mui/icons-material/Dashboard';
// import FormatListNumberedIcon from '@mui/icons-material/FormatListNumbered';
import { Link } from "react-router-dom";

import { useSessionStorage } from "../../hooks/useSessionStorage";


export const MenuItems = () => {

    let userRol = useSessionStorage('sessionTokenRol')
    return (
        <React.Fragment>
            { /*Dashboard Button */}
            <Link to='/katas'>
                <ListItemButton>
                    <ListItemIcon>
                        <DashboardIcon />
                    </ListItemIcon>
                    <ListItemText primary="Katas" />
                </ListItemButton>
            </Link>
            { /*Users */}
            {
                userRol !== 'admin' ?
                    null
                    :
                    <Link to='/users' >
                        <ListItemButton>
                            <ListItemIcon>
                                <PersonIcon />
                            </ListItemIcon>
                            <ListItemText primary="Users" />
                        </ListItemButton>
                    </Link>
            }

            { /*Ranking 
        <ListItemButton>
            <ListItemIcon>
                <FormatListNumberedIcon />
            </ListItemIcon>
            <ListItemText primary="Ranking" />
        </ListItemButton>
        */}
        </React.Fragment >
    )
}