import Container from "@mui/material/Container"
import Grid from "@mui/material/Grid"
import Paper from "@mui/material/Paper"
import React from "react"
import { Outlet } from "react-router-dom"

export const ContainerGrid = () => {
    return (
        <React.Fragment>
            <Container maxWidth='lg' sx={{ mt: 4, mg: 4 }}>
                <Grid item xs={12} md={12} lg={12} >
                    <Paper
                        sx={{
                            p: 2,
                            display: 'flex',
                            flexDirection: 'column',
                            height: 400
                        }}
                    >

                        <Outlet />
                    </Paper>
                </Grid>
            </Container>
        </React.Fragment >
    )
}