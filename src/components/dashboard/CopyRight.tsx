import { Typography } from "@mui/material";
import { Link } from "@mui/material";

export const Copyright = (props: any) => { 

    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'CopyRight © ' + new Date().getFullYear()+ ' '}
            <Link color="inherit" href="https://gitlab.com/sergiond">
                Sergio's Repo
            </Link> 
            
        </Typography>
    )
}