import React, { Fragment, useState } from 'react';
import Highlight, { defaultProps } from "prism-react-renderer";
import theme from 'prism-react-renderer/themes/nightOwl';
import Editor from 'react-simple-code-editor';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

interface EditorProps {
    language?: any,
    children?: any
}

const codeSnippet =
    `//This is an example but you can solve here the Kata 
//NOTE: be aware the blanks
    import axios from "axios";

    const getUser = () => {

        return axios.get('https://radomuser.me/api');
    }`

// Define Styles for Editor
const styles: any = {
    root: {
        boxSizing: 'border-box',
        fontFamily: '"Dank Mono", "Fira Code", monospace',
        ...theme.plain
    }
}

export const SolutionEditor = ({ language, children }: EditorProps) => {
    const [code, setCode] = useState<any>(codeSnippet);

    const handleCodeChange = (newCode: any) => {
        setCode(newCode)
    }

    const HighlightElement = (newCode: any) => (
        <Highlight {...defaultProps} theme={theme} code={children} language="typescript">
            {({ tokens, getLineProps, getTokenProps }) => (
                <Fragment>
                    {tokens.map((line, i) => (
                        <div {...getLineProps({ line, key: i })}>
                            {line.map((token, key) => <span {...getTokenProps({ token, key })} />)}
                        </div>
                    ))}
                </Fragment>
            )}
        </Highlight>
    )

    return (
        <Grid item sx={{ margin: 'auto', width: '500px', height: '500' }} xs={12} component={Paper} elevation={0} square >
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Editor
                    value={children}
                    onValueChange={handleCodeChange}
                    highlight={HighlightElement}
                    padding={30}
                    style={styles.root}
                />
            </Box>
        </Grid>
    )
}