import React, { Fragment, useState } from 'react';
import Editor from 'react-simple-code-editor';
import Highlight, { defaultProps } from 'prism-react-renderer';
import theme from 'prism-react-renderer/themes/nightOwl';
import { getKataByID, getKatasSolution, updateKata, updateKataStars } from '../../services/katasService';
import { useParams } from 'react-router-dom';
import { useSessionStorage } from '../../hooks/useSessionStorage';
import { AxiosResponse } from 'axios';

// Types
import { kata } from '../../utils/types/Kata.type';

// Material UI
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import { Grid, Paper, Box, Typography } from '@mui/material';
import { SolutionEditor } from './SolutionEditor';




interface EditorProps {
    language?: any,
    children?: any
}

const codeSnippet =
    `//This is an example but you can solve here the Kata 
//NOTE: be aware the blanks
    import axios from "axios";

    const getUser = () => {

        return axios.get('https://radomuser.me/api');
    }`
// Define Styles for Editor
const styles: any = {
    root: {
        boxSizing: 'border-box',
        fontFamily: '"Dank Mono", "Fira Code", monospace',
        ...theme.plain
    }
}


export const NewEditor = ({ children }: EditorProps) => {

    // Session JWT 
    let loggedIn = useSessionStorage('sessionJWTToken');
    let userID = useSessionStorage('sessionTokenID');
    // Fin id from params
    let { id } = useParams();


    const [code, setCode] = useState<any>(codeSnippet);
    const [kata, setKata] = useState<kata | any>(undefined);
    const [hideSolution, setHideSolution] = useState<boolean>(true);
    const [hideButtonSolution, setHideButtonSolution] = useState<boolean>(true);

    const handleCodeChange = (newCode: any) => {
        setCode(newCode)
    }

    const getKataDetails = () => {

        getKataByID(loggedIn, id).then((response: AxiosResponse) => {
            if (response.status === 200 && response.data) {
                let { _id, name, description, level, intents, stars, creator, solution, participants } = response.data
                let kataData: kata = {
                    _id: _id,
                    name: name,
                    description: description,
                    level: level,
                    intents: intents,
                    stars: stars,
                    creator: creator,
                    solution: solution,
                    participants: participants
                }
                setKata(kataData)
            }

        }).catch((error) => console.error(`[Kata by ID ERROR]: ${error}`))
    }


    const HighlightElement = (newCode: any) => (
        <Highlight {...defaultProps} theme={theme} code={newCode} language="typescript">
            {({ tokens, getLineProps, getTokenProps }) => (
                <Fragment>
                    {tokens.map((line, i) => (
                        <div {...getLineProps({ line, key: i })}>
                            {line.map((token, key) => <span {...getTokenProps({ token, key })} />)}
                        </div>
                    ))}
                </Fragment>
            )}
        </Highlight>
    )

    return (

        <Grid item sx={{ margin: 'auto', }} xs={12} component={Paper} elevation={0} square >
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Box>
                    <Editor
                        value={code}
                        onValueChange={handleCodeChange}
                        highlight={HighlightElement}
                        padding={10}
                        style={styles.root}
                    />
                </Box>
                <Box sx={{ mt: 2 }}>
                    <Button variant="contained" endIcon={<SendIcon />} onClick={() => getKatasSolution(loggedIn, id, code)
                        .then((response: AxiosResponse) => {
                            getKataDetails();
                            updateKata(
                                loggedIn,
                                id,
                                userID,
                                children.name,
                                children.description,
                                children.level,
                                children.creator,
                                children.solution,
                                'add',
                            )
                            console.table(kata)
                            setHideSolution(false);
                            if (response.data.status === 200) {
                                alert('The answer is correct')
                            } else {
                                alert('The answer is NOT correct, NOTE: be aware the blanks')
                                throw new Error('Answer incorrect, try again');

                            }
                        })
                        .catch((error) => console.error(`[Kata Solution ERROR]: ${error}`))
                    }
                    >
                        Solve Kata
                    </Button>
                </Box>
                <Box sx={{ mt: 4, p: 4 }}>

                    {
                        hideSolution ?
                            null
                            :
                            <Box>
                                <Box sx={{ mt: 4 }}>
                                    <Box>
                                        <Typography sx={{ mb: 1 }} component="h3" variant="h5">
                                            Rate this kata
                                        </Typography>
                                        <Rater
                                            total={5}
                                            rating={3}
                                            onRate={({ rating }) => {
                                                updateKataStars(loggedIn, id, rating).then((response: AxiosResponse) => {
                                                    alert('Thanks for rating')
                                                    window.location.reload();
                                                }).catch((error) => console.error(`[Kata by ID ERROR]: ${error}`))
                                            }}
                                        />
                                    </Box>
                                    <Box mt={4}>
                                        <Button sx={{ maxWidth: '150px' }} onClick={() => setHideButtonSolution(!hideButtonSolution)}
                                            color="primary"
                                            variant="outlined"
                                            size="medium"
                                            fullWidth>
                                            {hideButtonSolution ? 'Show Solution' : 'Hide Solution'}
                                        </Button>
                                    </Box>
                                </Box>
                                <Box mt={4}>
                                    {

                                        hideButtonSolution ?
                                            null
                                            :
                                            <Box >
                                                <SolutionEditor>
                                                    {kata?.solution}
                                                </SolutionEditor >
                                            </Box>

                                        // 
                                    }
                                </Box>
                            </Box>
                    }
                </Box>
            </Box>
        </Grid>
    )
}