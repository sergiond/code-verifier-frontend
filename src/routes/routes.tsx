import { Routes, Route, Navigate } from 'react-router-dom'

import { HomePage } from '../pages/HomePage';
import { LoginPage } from '../pages/LoginPage';
import { KatasPage } from '../pages/katasPage';
import { KatasDetailPage } from '../pages/KatasDetailPage';
import { UsersPage } from '../pages/UsersPage';
import { UserDetailPage } from '../pages/UsersDetailPage';

export const AppRoutes = () => {
    return (
        <Routes>
            { /*Routes definition */}
            <Route path='/' element={<HomePage />}>
                <Route path='/' element={<Navigate to='/katas' replace />}></Route>
                <Route path='/katas' element={<KatasPage />}></Route>
                <Route path='/katas/:id' element={<KatasDetailPage />}></Route>
                <Route path='/users' element={<UsersPage />}></Route>
                <Route path='/users/:id' element={<UserDetailPage />}></Route>
            </Route>
            <Route path='/login' element={<LoginPage />}></Route>
            {/* Redirect to homepage when page Not fount */}
            <Route path='*' element={<Navigate to='/' replace />}></Route>
        </Routes>
    )

}