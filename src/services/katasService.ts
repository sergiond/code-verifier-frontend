import { AxiosRequestConfig } from 'axios'
import axios from '../utils/config/axios.config'

export const getAllKatas = (token: string, limit?: number, page?: number) => { 

    // Petition to http://localhost/api/katas?limit=1&?page1
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            limit: limit,
            page: page
        }
    }

    return axios.get('/katas', options)
}

export const getKataByID = (token: string, id?: string) => { 

      // Petition to http://localhost/api/katas?id=xxxxxxxxxxxx
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            id: id
        }
    }

    return axios.get('/katas', options)
}

export const getKatasSolution = (token: string, id?: string, answer?: string) => { 

      // Petition to http://localhost/api/katas/katasolution?id=xxxxxxxxxxxx
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            id: id
        }
    }

    let body = { answer: answer }

    console.log(body)
    return axios.post('/katas/katasolution', body, options)
}

export const deleteKataByID = (token: string, id: string) => { 

      // Petition to http://localhost/api/katas?id=xxxxxxxxxxxx
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            id: id
        }
    }
    return axios.delete('/katas', options)
}

export const createKata = (
    token: string,
    name: string,
    description: string,
    level: string,
    intents: number,
    stars: number,
    solution: string
) => { 

    // Petition to http://localhost/api/katas
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        }
    }

    let body = {
        name: name,
        description: description,
        level: level,
        intents: intents,
        stars: stars,
        solution: solution
    }

    return axios.post('/katas',body, options )
}

export const updateKata = (
    token: string,
    id: any,
    userid?: any,
    name?: string,
    description?: string,
    level?: string,
    creator?: string,
    solution?: string,
    action?: string
) => { 

      // Petition to http://localhost/api/katas?id=xxxxxxxxxxxx
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
            'sessionTokenID': userid
        },
        params: {
            id: id
        },
    }

    let body = {
            name: name,
            description: description,
            level: level,
            creator: creator,
            solution: solution,
            action: action
    }
    return axios.put('/katas', body, options)
}

export const updateKataStars = (token: string, id: any, stars: number) => { 

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            id: id
        }
    }
    let body = {
        stars: stars
    }
    console.log(body.stars)
    return axios.put('/katas/updatestars', body, options)
}