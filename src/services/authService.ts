import axios from '../utils/config/axios.config'

/**
 * Login Method
 * @param {string} email  Email to login an user
 * @param {string} password Password to login an user
 * @returns 
 */
export const login = (email: string, password: string) => { 
    
    // Declare Body to POST
    let body = {
        email: email,
        password: password
    }

    // Send POST request to login endpoint and return the promise
    // http://localhost:8000/api/auth/login
    return axios.post('/auth/login', body)
}

/**
 * Register Method
 * @param {string} name  Users name
 * @param {string} email  Email to register a new user
 * @param {string} password Password to register a new user
 * @param {number} age Users Age
 * @returns 
 */
export const register = (name: string, email: string, password: string, rol:string,  age: number) => { 
    
    // Declare Body to POST
    let body = {
        name: name,
        email: email,
        password: password,
        rol: rol,
        age: age
    }

    // Send POST request to regist endpoint and return the promise
    // http://127.0.0.1:8000/api/auth/register
    return axios.post('/auth/register', body)
}