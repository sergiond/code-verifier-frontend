import { AxiosRequestConfig } from 'axios'
import axios from '../utils/config/axios.config'

/**
 * Petición para modificar usuario y añadir ID de la Kata
 */
export const getAllUsers = (token: string, limit?: number, page?: number) => { 

    // Petition to http://localhost/api/users?limit=1&?page1
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            limit: limit,
            page: page
        }
    }
    return axios.get('/users', options)
}

    export const getUserByID = (token: string, id?: string) => { 

      // Petition to http://localhost/api/katas?id=xxxxxxxxxxxx
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            id: id
        }
    }

    return axios.get('/users', options)
}

export const updateUserById = (token: string, id: string, name: string, email: string, password: string, rol:string,  age: number)=> {

    // Petition to http://localhost/api/users?id=xxxxxxxxxxxx
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            id: id
        }
    }
    
    let body = {
        name: name,
        email: email,
        password: password,
        rol: rol,
        age: age,
    }
    console.table(options)
    console.table(body)
    return axios.put('/users/update', body, options)
}

export const updateUserKatasById = (token: string, id?: string, action?: string, kataid?: string) => { 

      // Petition to http://localhost/api/users?id=xxxxxxxxxxxx
    // add header with JWT in x-acess-token

    const options: AxiosRequestConfig = {
        headers: {
            'x-access-token': token,
        },
        params: {
            id: id
        }
    }
    
    let body = {
        action: action,
        katas: kataid
    }
    console.table(options)
    console.table(body)
    return axios.put('/users', body ,options)
}